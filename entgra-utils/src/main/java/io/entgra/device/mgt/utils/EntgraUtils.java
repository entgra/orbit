package io.entgra.device.mgt.utils;

import java.io.File;

public class EntgraUtils {

    public static String getEntgraHome() {
        String entgraHome = System.getProperty("entgra.home");
        if (entgraHome == null) {
            entgraHome = System.getenv("ENTGRA_HOME");
            System.setProperty("entgra.home", entgraHome);
        }
        return entgraHome;
    }

    public static String getConfDirectory() {
        String entgraHome = getEntgraHome();
        return entgraHome + File.separator + "conf" + File.separator;
    }

}
