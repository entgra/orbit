
/*
*  Copyright (c) 2005-2010, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
*
*  WSO2 Inc. licenses this file to you under the Apache License,
*  Version 2.0 (the "License"); you may not use this file except
*  in compliance with the License.
*  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package org.wso2.carbon.user.core.common;

import io.entgra.cache.guava.wrapper.GuavaCachingProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.user.core.UserCoreConstants;
import org.wso2.carbon.user.core.authorization.AuthorizationCache;
import org.wso2.carbon.user.core.authorization.AuthorizationKey;
import org.wso2.carbon.user.core.authorization.AuthorizeCacheEntry;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.spi.CachingProvider;

public class UserRolesCache {

    private static final String USER_ROLES_CACHE = "USER_ROLES_CACHE";
    private static final String CASE_INSENSITIVE_USERNAME = "CaseInsensitiveUsername";
    private static Log log = LogFactory.getLog(UserRolesCache.class);
    private static UserRolesCache userRolesCache = new UserRolesCache();
    private static final Object lock = new Object();


    private int timeOut = UserCoreConstants.USER_ROLE_CACHE_DEFAULT_TIME_OUT;

    private UserRolesCache() {
        MutableConfiguration<UserRolesCacheKey, UserRolesCacheEntry> configuration = new MutableConfiguration<>();
        configuration.setStoreByValue(false);
        configuration.setTypes(UserRolesCacheKey.class, UserRolesCacheEntry.class);

        CachingProvider cachingProvider = Caching.getCachingProvider(GuavaCachingProvider.class.getName());
        CacheManager cacheManager = cachingProvider.getCacheManager();
        cacheManager.createCache(USER_ROLES_CACHE, configuration);
    }

    /**
     * Gets a new instance of UserRolesCache.
     *
     * @return A new instance of UserRolesCache.
     */
    public static UserRolesCache getInstance() {
        if (userRolesCache == null) {
            synchronized (lock) {
                userRolesCache = new UserRolesCache();
            }
        }
        return userRolesCache;
    }


    /**
     * Getting existing cache if the cache available, else returns a newly created cache.
     * This logic handles by javax.cache implementation
     */
    private Cache<UserRolesCacheKey, UserRolesCacheEntry> getUserRolesCache() {
        CachingProvider cachingProvider = Caching.getCachingProvider(GuavaCachingProvider.class.getName());
        return cachingProvider.getCacheManager().getCache(USER_ROLES_CACHE);
    }

    /**
     * Avoiding NullPointerException when the cache is null
     *
     * @return boolean whether given cache is null
     */
    private boolean isCacheNull(Cache<UserRolesCacheKey, UserRolesCacheEntry> cache) {
        if (cache == null) {
            if (log.isDebugEnabled()) {
                StackTraceElement[] elemets = Thread.currentThread().getStackTrace();
                String traceString = "";
                for (int i = 1; i < elemets.length; ++i) {
                    traceString += elemets[i] + System.getProperty("line.separator");
                }
                log.debug("USER_ROLES_CACHE doesn't exist in CacheManager:\n" + traceString);
            }
            return true;
        }
        return false;
    }

    //add to cache
    public void addToCache(String serverId, int tenantId, String userName, String[] userRoleList) {

        Cache<UserRolesCacheKey, UserRolesCacheEntry> cache = this.getUserRolesCache();
        //check for null
        if (isCacheNull(cache)) {
            return;
        }
        if (!isCaseSensitiveUsername(userName, tenantId)) {
            userName = userName.toLowerCase();
        }
        //create cache key
        UserRolesCacheKey userRolesCacheKey = new UserRolesCacheKey(serverId, tenantId, userName);
        //create cache entry
        UserRolesCacheEntry userRolesCacheEntry = new UserRolesCacheEntry(userRoleList);
        //add to cache
        cache.put(userRolesCacheKey, userRolesCacheEntry);

    }

    //get roles list of user
    public String[] getRolesListOfUser(String serverId, int tenantId, String userName) {

        Cache<UserRolesCacheKey, UserRolesCacheEntry> cache = this.getUserRolesCache();
        //check for null
        if (isCacheNull(cache)) {
            return new String[0];
        }
        if (!isCaseSensitiveUsername(userName, tenantId)) {
            userName = userName.toLowerCase();
        }
        //create cache key
        UserRolesCacheKey userRolesCacheKey = new UserRolesCacheKey(serverId, tenantId, userName);
        //search cache and get cache entry
        UserRolesCacheEntry userRolesCacheEntry = cache.get(userRolesCacheKey);

        if (userRolesCacheEntry == null) {
            return new String[0];
        }

        return userRolesCacheEntry.getUserRolesList();
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    // lear userRolesCache by tenantId
    public void clearCacheByTenant(int tenantId) {

        Cache<UserRolesCacheKey, UserRolesCacheEntry> cache = this.getUserRolesCache();
        cache.removeAll();
    }

    // Clear userRolesCache by serverId, tenant and user name
    public void clearCacheEntry(String serverId, int tenantId, String userName) {

        Cache<UserRolesCacheKey, UserRolesCacheEntry> cache = getUserRolesCache();
        // Check for null
        if (isCacheNull(cache)) {
            return;
        }

        boolean caseSensitiveUsername = isCaseSensitiveUsername(userName, tenantId);
        if (!caseSensitiveUsername) {
            userName = userName.toLowerCase();
        }
        UserRolesCacheKey userRolesCacheKey = new UserRolesCacheKey(serverId, tenantId, userName);
        if (cache.containsKey(userRolesCacheKey)) {
            cache.remove(userRolesCacheKey);
        }

        String userNameWithCacheIdentifier = UserCoreConstants.IS_USER_IN_ROLE_CACHE_IDENTIFIER + userName;

        // creating new key for isUserHasRole cache.
        if(!caseSensitiveUsername) {
            userNameWithCacheIdentifier = userNameWithCacheIdentifier.toLowerCase();
        }

        userRolesCacheKey = new UserRolesCacheKey(serverId, tenantId, userNameWithCacheIdentifier);
        if (cache.containsKey(userRolesCacheKey)) {
            cache.remove(userRolesCacheKey);
        }
    }


    private boolean isCaseSensitiveUsername(String username, int tenantId) {
        return true;
    }

    private String removeUserInRoleIdentifier(String modifiedName) {
        String originalName = modifiedName;
        if (originalName.contains(UserCoreConstants.IS_USER_IN_ROLE_CACHE_IDENTIFIER)) {
            originalName = modifiedName.replace(UserCoreConstants.IS_USER_IN_ROLE_CACHE_IDENTIFIER, "");
        }
        return originalName;
    }
}
