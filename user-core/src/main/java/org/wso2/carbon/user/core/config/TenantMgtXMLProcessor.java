/*
*  Copyright (c) 2005-2010, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
*
*  WSO2 Inc. licenses this file to you under the Apache License,
*  Version 2.0 (the "License"); you may not use this file except
*  in compliance with the License.
*  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package org.wso2.carbon.user.core.config;

import io.entgra.device.mgt.utils.EntgraUtils;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.user.api.TenantMgtConfiguration;
import org.wso2.carbon.user.core.UserCoreConstants;
import org.wso2.carbon.user.core.UserStoreException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This reads the tenant-config.xml through axiom api and constructs an object of
 * TenantMgtConfiguration
 */
public class TenantMgtXMLProcessor {

    private static final String TENANT_MGT_XML = "tenant-mgt.xml";
    private static Log log = LogFactory.getLog(TenantMgtXMLProcessor.class);

    public void setBundleContext() {
    }

    /**
     * Build an object of TenantMgtConfiguration reading the tenant-mgt.xml file
     *
     * @return
     * @throws UserStoreException
     */
    public TenantMgtConfiguration buildTenantMgtConfigFromFile(String tenantManagerClass)
            throws UserStoreException {

        try {
            OMElement tenantMgtConfigElement = getTenantMgtConfigElement();
            return buildTenantMgtConfiguration(tenantMgtConfigElement, tenantManagerClass);
        } catch (XMLStreamException e) {
            String error_Message = "Error in reading tenant-mgt.xml";
            if (log.isDebugEnabled()) {
                log.debug(error_Message, e);
            }
            throw new UserStoreException(error_Message);
        } catch (IOException e) {
            String error_Message = "Error in reading tenant-mgt.xml file.";
            if (log.isDebugEnabled()) {
                log.debug(error_Message, e);
            }
            throw new UserStoreException(error_Message);
        }
    }

    /**
     * Build the tenant configuration given the document element in tenant-mgt.xml
     *
     * @param tenantMgtConfigElement
     * @return
     * @throws UserStoreException
     */
    public TenantMgtConfiguration buildTenantMgtConfiguration(OMElement tenantMgtConfigElement, String tenantManagerClass)
            throws UserStoreException {
        Map<String, String> tenantMgtProperties = null;
        TenantMgtConfiguration tenantMgtConfiguration = new TenantMgtConfiguration();

        Iterator<OMElement> iterator = tenantMgtConfigElement.getChildrenWithName(
                new QName(UserCoreConstants.TenantMgtConfig.LOCAL_NAME_TENANT_MANAGER));

        for (; iterator.hasNext(); ) {
            OMElement tenantManager = iterator.next();

            if (tenantManagerClass != null && tenantManagerClass.equals(tenantManager.getAttributeValue(new QName(
                    UserCoreConstants.TenantMgtConfig.ATTRIBUTE_NAME_CLASS)))) {

                tenantMgtProperties = readChildPropertyElements(tenantManager);

                tenantMgtConfiguration.setTenantManagerClass(tenantManagerClass);
                tenantMgtConfiguration.setTenantStoreProperties(tenantMgtProperties);

                return tenantMgtConfiguration;
            }
        }
        String errorMessage = "Error in locating TenantManager compatible with PrimaryUserStore."
                + " Required a TenantManager using " + tenantManagerClass + " in tenant-mgt.xml.";
        if (log.isDebugEnabled()) {
            log.debug(errorMessage);
        }
        throw new UserStoreException(errorMessage);
    }

    private Map<String, String> readChildPropertyElements(OMElement parentElement) {

        Map<String, String> tenantMgtConfigProperties = new HashMap<String, String>();
        Iterator ite = parentElement.getChildrenWithName(new QName(
                UserCoreConstants.TenantMgtConfig.LOCAL_NAME_PROPERTY));
        while (ite.hasNext()) {
            OMElement propertyElement = (OMElement) ite.next();
            String propertyName = propertyElement.getAttributeValue(new QName(
                    UserCoreConstants.TenantMgtConfig.ATTR_NAME_PROPERTY_NAME));
            String propertyValue = propertyElement.getText();
            tenantMgtConfigProperties.put(propertyName, propertyValue);
        }
        return tenantMgtConfigProperties;
    }

    private OMElement getTenantMgtConfigElement() throws IOException, XMLStreamException {
        InputStream inStream = null;
        try {
            String tenantMgt = EntgraUtils.getConfDirectory() + TENANT_MGT_XML;
            if (tenantMgt != null) {
                File tenantMgtXml = new File(tenantMgt);
                if (!tenantMgtXml.exists()) {
                    String msg = "Instance of a WSO2 User Manager has not been created. tenant-mgt.xml is not found.";
                    throw new FileNotFoundException(msg);
                }
                inStream = new FileInputStream(tenantMgtXml);
            } else {
                inStream = this.getClass().getClassLoader()
                        .getResourceAsStream("repository/conf/tenant-mgt.xml");
                if (inStream == null) {
                    String msg = "Instance of a WSO2 User Manager has not been created. tenant-mgt.xml is not found. Please set the entgra.home";
                    throw new FileNotFoundException(msg);
                }
            }
            StAXOMBuilder builder = new StAXOMBuilder(inStream);
            OMElement documentElement = builder.getDocumentElement();
            Iterator<OMElement> elements = documentElement.getChildElements();
            while(elements.hasNext()) {
                elements.next(); //allows to close the stream here itself
            }
            return documentElement;
        } catch (IOException | XMLStreamException e) {
            log.error("Error while reading claim config:" + e.getMessage(), e);
            throw e;
        } finally {
            if (inStream != null) {
                inStream.close();
            }
        }
    }
}
