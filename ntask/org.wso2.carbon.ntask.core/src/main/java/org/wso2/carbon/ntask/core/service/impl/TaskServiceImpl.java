/**
 *  Copyright (c) 2011, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.wso2.carbon.ntask.core.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.w3c.dom.Document;
import org.wso2.carbon.context.PrivilegedCarbonContext;
import org.wso2.carbon.ntask.common.TaskException;
import org.wso2.carbon.ntask.common.TaskException.Code;
import org.wso2.carbon.ntask.core.TaskManager;
import org.wso2.carbon.ntask.core.TaskManagerFactory;
import org.wso2.carbon.ntask.core.TaskManagerId;
import org.wso2.carbon.ntask.core.TaskUtils;
import org.wso2.carbon.ntask.core.impl.QuartzCachedThreadPool;
import org.wso2.carbon.ntask.core.impl.standalone.StandaloneTaskManagerFactory;
import org.wso2.carbon.ntask.core.internal.TasksDSComponent;
import org.wso2.carbon.ntask.core.service.TaskService;
import org.wso2.carbon.ntask.core.service.impl.TaskServiceXMLConfiguration.DefaultLocationResolver;
import org.wso2.carbon.ntask.core.service.impl.TaskServiceXMLConfiguration.DefaultLocationResolver.Property;
import org.wso2.carbon.utils.CarbonUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class represents the TaskService implementation.
 * @see TaskService
 */
public class TaskServiceImpl implements TaskService {

    private static final Log log = LogFactory.getLog(TaskServiceImpl.class);

    private Set<String> registeredTaskTypes;

    private boolean serverInit;

    private TaskManagerFactory taskManagerFactory;

    private TaskServiceConfiguration taskServerConfiguration;
    
    private TaskServerMode effectiveTaskServerMode;

    private static final String QUARTZ_PROPERTIES_FILE_NAME = "quartz.properties";

    private static ExecutorService executor = Executors.newCachedThreadPool();

    private Scheduler scheduler;

    public TaskServiceImpl() throws RuntimeException {

        //TODO:Read Quartz properties file from entgra home and validate this whole method
        try {
            this.registeredTaskTypes = new HashSet<String>();
            this.taskServerConfiguration = new TaskServiceConfigurationImpl(this.loadTaskServiceXMLConfig());
            this.taskManagerFactory = new StandaloneTaskManagerFactory();
            this.effectiveTaskServerMode = TaskServerMode.STANDALONE;

            if (executor.isShutdown()) {
                executor = Executors.newCachedThreadPool();
            }

            String quartzConfigFilePath = CarbonUtils.getCarbonConfigDirPath() + File.separator
                    + "etc" + File.separator + QUARTZ_PROPERTIES_FILE_NAME;

            StdSchedulerFactory fac;
            if (new File(quartzConfigFilePath).exists()) {
                fac = new StdSchedulerFactory(quartzConfigFilePath);
            } else {
                fac = new StdSchedulerFactory(this.getStandardQuartzProps());
            }
            scheduler = fac.getScheduler();
            scheduler.start();
        } catch (TaskException e) {
            log.error("Failed to start TaskService. Server startup will fail. " + e.getMessage(), e);
            throw new RuntimeException("Failed to start TaskService. Server startup will fail. " + e.getMessage(), e);
        } catch (SchedulerException e) {
            log.error("Failed to start TaskService. Server startup will fail. " + e.getMessage(), e);
            throw new RuntimeException("Failed to start TaskService. Server startup will fail. " + e.getMessage(), e);
        }

        log.info("Task service starting in " + this.getEffectiveTaskServerMode() + " mode...");
    }

    private Properties getStandardQuartzProps() {
        Properties result = new Properties();
        result.put("org.quartz.scheduler.skipUpdateCheck", "true");
        result.put("org.quartz.threadPool.class", QuartzCachedThreadPool.class.getName());
        return result;
    }

    private TaskServiceXMLConfiguration loadTaskServiceXMLConfig() throws TaskException {
        String path = CarbonUtils.getCarbonConfigDirPath() + File.separator + "etc"
                + File.separator + "tasks-config.xml";
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        Document doc = TaskUtils.convertToDocument(file);
        TaskUtils.secureResolveDocument(doc);
        JAXBContext ctx;
        try {
            ctx = JAXBContext.newInstance(TaskServiceXMLConfiguration.class);
            TaskServiceXMLConfiguration taskConfig = (TaskServiceXMLConfiguration) ctx
                    .createUnmarshaller().unmarshal(doc);
            return taskConfig;
        } catch (JAXBException e) {
            throw new TaskException(e.getMessage(), Code.CONFIG_ERROR, e);
        }

    }

    @Override
    public boolean isServerInit() {
        return serverInit;
    }

    public TaskManagerFactory getTaskManagerFactory() {
        return taskManagerFactory;
    }

    @Override
    public Set<String> getRegisteredTaskTypes() {
        return registeredTaskTypes;
    }

    private void initTaskManagersForType(String taskType) throws TaskException {
        if (log.isDebugEnabled()) {
            log.debug("Initializing task managers [" + taskType + "]");
        }
        List<TaskManager> startupTms = this.getTaskManagerFactory()
                .getStartupSchedulingTaskManagersForType(taskType);
        for (TaskManager tm : startupTms) {
            tm.initStartupTasks();
        }
    }

    @Override
    public Scheduler getScheduler() {
        return this.scheduler;
    }

    @Override
    public TaskManager getTaskManager(String taskType) throws TaskException {
        int tenantId = PrivilegedCarbonContext.getThreadLocalCarbonContext().getTenantId(true);
        return this.getTaskManagerFactory().getTaskManager(
                new TaskManagerId(tenantId, taskType));
    }

    @Override
    public List<TaskManager> getAllTenantTaskManagersForType(String taskType) throws TaskException {
        return this.getTaskManagerFactory().getAllTenantTaskManagersForType(taskType);
    }

    @Override
    public synchronized void registerTaskType(String taskType) throws TaskException {
        this.registeredTaskTypes.add(taskType);
        /* if server has finished initializing, lets initialize the
         * task managers for this type */
        if (this.isServerInit()) {
            this.initTaskManagersForType(taskType);
        }
    }

    @Override
    public synchronized void serverInitialized() {
        try {
            this.serverInit = true;
            for (String taskType : this.getRegisteredTaskTypes()) {
                this.initTaskManagersForType(taskType);
            }            
        } catch (TaskException e) {
            String msg = "Error initializing task managers: " + e.getMessage();
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        }
    }

    @Override
    public TaskServiceConfiguration getServerConfiguration() {
        return taskServerConfiguration;
    }

    private class TaskServiceConfigurationImpl implements TaskServiceConfiguration {

        private TaskServerMode taskServerMode;

        private int taskServerCount = -1;

        private String taskClientDispatchAddress;

        private String remoteServerAddress;

        private String remoteServerUsername;

        private String remoteServerPassword;

        private String locationResolverClass;
        
        private Map<String, String> locationResolverProperties;

        public TaskServiceConfigurationImpl(TaskServiceXMLConfiguration taskXMLConfig) {
            this.processXMLConfig(taskXMLConfig);
            this.processSystemProps();
        }

        private void processXMLConfig(TaskServiceXMLConfiguration taskXMLConfig) {
            if (taskXMLConfig == null) {
                return;
            }
            this.taskClientDispatchAddress = taskXMLConfig.getTaskClientDispatchAddress();
            this.remoteServerAddress = taskXMLConfig.getRemoteServerAddress();
            this.remoteServerUsername = taskXMLConfig.getRemoteServerUsername();
            this.remoteServerPassword = taskXMLConfig.getRemoteServerPassword();
            this.taskServerMode = taskXMLConfig.getTaskServerMode();
            this.taskServerCount = taskXMLConfig.getTaskServerCount();
            DefaultLocationResolver locationResolver = taskXMLConfig.getDefaultLocationResolver();
            this.locationResolverClass = locationResolver.getLocationResolverClass();
            this.locationResolverProperties = this.extractLocationResolverProperties(locationResolver);
        }
        
        private Map<String, String> extractLocationResolverProperties(DefaultLocationResolver locationResolver) {
        	Map<String, String> result = new HashMap<String, String>();
        	Property[] props = locationResolver.getProperties();
        	if (props != null) {
        		for (Property prop : props) {
        			result.put(prop.getName(), prop.getValue());
        		}
        	}
        	return result;
        }

        private void processSystemProps() {

            if (this.taskServerMode == null) {
                this.taskServerMode = TaskServerMode.AUTO;
                
            }
            if (this.taskServerCount == -1) {
                this.taskServerCount = -1;
            }
        }

        private String returnSystemPropValueIfValid(String originalValue, String sysPropName) {
            String sysPropValue = System.getProperty(sysPropName);
            if (sysPropValue != null) {
                return sysPropValue;
            } else {
                return originalValue;
            }
        }

        @Override
        public String getTaskClientDispatchAddress() {
            return taskClientDispatchAddress;
        }

        @Override
        public TaskServerMode getTaskServerMode() {
            return taskServerMode;
        }

        @Override
        public String getRemoteServerAddress() {
            return remoteServerAddress;
        }

        @Override
        public String getRemoteServerUsername() {
            return remoteServerUsername;
        }

        @Override
        public String getRemoteServerPassword() {
            return remoteServerPassword;
        }

        @Override
        public int getTaskServerCount() {
            return taskServerCount;
        }

        @Override
        public String getLocationResolverClass() {
            if (locationResolverClass == null) {
                return TaskServiceXMLConfiguration.DEFAULT_LOCATION_RESOLVER_CLASS;
            }
            return locationResolverClass;
        }

		@Override
		public Map<String, String> getLocationResolverProperties() {
			return locationResolverProperties;
		}

    }

    public TaskServerMode getEffectiveTaskServerMode() {
        return effectiveTaskServerMode;
    }

    @Override
    @Deprecated
    public void runAfterRegistrationActions() throws TaskException {
        if (this.getEffectiveTaskServerMode() == TaskServerMode.CLUSTERED) {
            // Do nothing as no clusstered task
            // for (String taskType : this.getRegisteredTaskTypes()) {
            //     ClusterGroupCommunicator.getInstance(taskType).checkServers();
            //}
        }
    }

}
