/**
 *  Copyright (c) 2011, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.wso2.carbon.ntask.core.impl;


import org.wso2.carbon.ntask.common.TaskException;
import org.wso2.carbon.ntask.core.TaskInfo;
import org.wso2.carbon.ntask.core.TaskManagerId;
import org.wso2.carbon.ntask.core.TaskRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Registry based task repository implementation. This is a DAO object.
 */
public class DBBasedTaskRepository implements TaskRepository {

    public static final String REG_TASK_BASE_PATH = "/repository/components/org.wso2.carbon.tasks";

    public static final String REG_TASK_REPO_BASE_PATH = REG_TASK_BASE_PATH + "/" + "definitions";

    private String taskType;

    private static Marshaller taskMarshaller;

    private static Unmarshaller taskUnmarshaller;

    private int tenantId;

    private static Map<String, TaskInfo> taskMap = new HashMap<String, TaskInfo>();

    private static Map<String, String> taskMetaData = new HashMap<>();

    private static Map<String, Integer> taskTypeData = new HashMap<>();

    private static int taskTypeId = 1;

    static {
        try {
            JAXBContext ctx = JAXBContext.newInstance(TaskInfo.class);
            taskMarshaller = ctx.createMarshaller();
            taskUnmarshaller = ctx.createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException("Error creating task marshaller/unmarshaller: "
                    + e.getMessage());
        }
    }

    public DBBasedTaskRepository(int tenantId, String taskType) throws TaskException {
        this.tenantId = tenantId;
        this.taskType = taskType;
    }

    @Override
    public int getTenantId() {
        return tenantId;
    }

    private static Marshaller getTaskMarshaller() {
        return taskMarshaller;
    }

    private static Unmarshaller getTaskUnmarshaller() {
        return taskUnmarshaller;
    }

    public String getTaskType() {
        return taskType;
    }

    @Override
    public List<TaskInfo> getAllTasks() throws TaskException {
    	/* a set is used here to exclude any possible duplicates */
        List<TaskInfo> tasks = new ArrayList(taskMap.values());
        return tasks;
    }

    @Override
    public TaskInfo getTask(String taskName) throws TaskException {
        return taskMap.get(taskName);
    }

    @Override
    public synchronized void addTask(TaskInfo taskInfo) throws TaskException {
        if (taskTypeData.get(taskType) == null) {
            taskTypeData.put(taskType, Integer.valueOf(taskTypeId++)); //TODO:taskTypeData addition, similar to registry
        } else {
            //do nothing
        }
        taskMap.put(taskInfo.getName(), taskInfo);
    }

    @Override
    public synchronized boolean deleteTask(String taskName) throws TaskException {
        taskMap.remove(taskName);
        return true;
    }


    @Override
    public String getTasksType() {
        return taskType;
    }

    public static List<TaskManagerId> getAvailableTenantTasksInRepo() throws TaskException {
        List<TaskManagerId> tmList = new ArrayList<TaskManagerId>();
        taskTypeData.entrySet().forEach( p -> { tmList.add(new TaskManagerId(p.getValue(), p.getKey())); });
        return tmList;
    }

    public static List<TaskManagerId> getAllTenantTaskManagersForType(String taskType)
            throws TaskException {
        return getAvailableTenantTasksInRepo();
    }


    @Override
    public void setTaskMetadataProp(String taskName, String key, String value) throws TaskException {
        taskMetaData.put(taskName + "-" + key, value);
    }

    @Override
    public String getTaskMetadataProp(String taskName, String key) throws TaskException {
        return taskMetaData.get(taskName + "-" + key);
    }

}
